
#include <DHT.h>
#include <DHT_U.h>

#include <ESP8266WiFi.h>

char *ssid = "Capteur Thermique"; // The name of the Wi-Fi network that will be created
char *password = "Mdp12345";   // The password required to connect to it, leave blank for an open network

WiFiServer server(80);

#define DHTPIN 2     // Digital pin connected to the DHT sensor 
// Feather HUZZAH ESP8266 note: use pins 3, 4, 5, 12, 13 or 14 --
// Pin 15 can work but DHT must be disconnected during program upload.

// Uncomment the type of sensor in use:
#define DHTTYPE    DHT11     // DHT 11
//#define DHTTYPE    DHT22     // DHT 22 (AM2302)
//#define DHTTYPE    DHT21     // DHT 21 (AM2301)

DHT dht(DHTPIN, DHTTYPE);

// --- Setup function ---
void setup(){
  
  Serial.begin(9600);
  delay(10);
  Serial.println('\n');
  
  // --- Initialize dht device ---
  
  dht.begin();

  /*
  Serial.println(F("DHTxx Unified Sensor Example"));
  // Print temperature sensor details.
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  Serial.println(F("------------------------------------"));
  Serial.println(F("Temperature Sensor"));
  Serial.print  (F("Sensor Type: ")); Serial.println(sensor.name);
  Serial.print  (F("Driver Ver:  ")); Serial.println(sensor.version);
  Serial.print  (F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  Serial.print  (F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("°C"));
  Serial.print  (F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("°C"));
  Serial.print  (F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("°C"));
  Serial.println(F("------------------------------------"));
  // Print humidity sensor details.
  dht.humidity().getSensor(&sensor);
  Serial.println(F("Humidity Sensor"));
  Serial.print  (F("Sensor Type: ")); Serial.println(sensor.name);
  Serial.print  (F("Driver Ver:  ")); Serial.println(sensor.version);
  Serial.print  (F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  Serial.print  (F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("%"));
  Serial.print  (F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("%"));
  Serial.print  (F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("%"));
  Serial.println(F("------------------------------------"));
  // Set delay between sensor readings based on sensor details.
  delayMS = sensor.min_delay / 1000;
   */
  // --- Initialize Wifi Access point and Server ---
 
  WiFi.softAP(ssid, password);             // Start the access point
  Serial.print("Access Point \"");
  Serial.print(ssid);
  Serial.println("\" started");

  IPAddress ip(192, 168, 1, 10); // where xx is the desired IP Address
  IPAddress gateway(192, 168, 1, 1); // set gateway to match your network
  IPAddress subnet(255, 255, 255, 0); // set subnet mask to match your network
  WiFi.softAPConfig(ip, gateway, subnet);

  Serial.print("IP address:\t");
  Serial.println(WiFi.softAPIP());         // Send the IP address of the ESP8266 to the computer

  server.begin();
  Serial.print("Server started");
  
}
// --- End of Setup function ---


// --- Loop function ---
void loop(){
  WiFiClient client = server.available();
  // wait for a client to connect
  if (client) {
    Serial.println("\n[Client connected]");
    while (client.connected()) {
      String line = client.readStringUntil('\r');
      Serial.print(line);
      if (line.indexOf("Temperature") >= 0) {
        float t = dht.readTemperature();
        client.println("Temperature");
        client.println(t); 
      }
      else if (line.indexOf("Humidite") >= 0) {
        float t = dht.readHumidity();
        client.println("Humidite");
        client.println(t); 
      }
      break;
      delay(1); // give the web server time to receive the data
      // close the connection:
      client.stop();
      Serial.println("[Client disconnected]");
    }  
  }
}
// --- End of Loop function ---
