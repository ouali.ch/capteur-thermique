
#include <Adafruit_NeoPixel.h>

#include <ESP8266WiFi.h>        // Include the Wi-Fi library

// --- Neopixels ---
// Which pin on the Arduino is connected to the NeoPixels?
#define LED_PIN    D4

// How many NeoPixels are attached to the Arduino?
#define LED_COUNT 60

// Declare our NeoPixel strip object:
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
// Argument 1 = Number of pixels in NeoPixel strip
// Argument 2 = Arduino pin number (most are valid)
// Argument 3 = Pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)

// --- Pushbutton ---
const int buttonPin = D2;
int buttonState = 0;         // variable for reading the pushbutton status

// --- WiFi ---
char* ssid     = "Capteur Thermique";         // The SSID (name) of the Wi-Fi network you want to connect to
char* password = "Mdp12345";     // The password of the Wi-Fi network

// --- Sensor selection and reading ---
int tempInt = 0;
String sensorChoice = "Temperature";
int sensorInfo = 0;

// --- Server ---
IPAddress server(192,168,1,10);


// --- Setup function ---
void setup() {
  Serial.begin(9600);         // Start the Serial communication to send messages to the computer
  delay(10);
  Serial.println('\n');

  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);

  // --- Initialize neopixel ---

  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();            // Turn OFF all pixels ASAP
  strip.setBrightness(9); // Set BRIGHTNESS to about 1/5 (max = 255)

  // --- Initialize LEDs for sensor selection --- 
  pinMode(D8, OUTPUT);
  pinMode(D7, OUTPUT);
  digitalWrite(D8, LOW);
  digitalWrite(D7, LOW);
  
  // --- Initialize Wifi station ---
  
  WiFi.begin(ssid, password);             // Connect to the network
  Serial.print("Connecting to ");
  Serial.print(ssid); Serial.println(" ...");

  int i = 0;
  while (WiFi.status() != WL_CONNECTED) { // Wait for the Wi-Fi to connect
    delay(1000);
    Serial.print(++i); Serial.print(' ');
  }

  Serial.println('\n');
  Serial.println("Connection established!");

  IPAddress ip(192, 168, 1, 20); // where xx is the desired IP Address
  IPAddress gateway(192, 168, 1, 1); // set gateway to match your network
  IPAddress subnet(255, 255, 255, 0); // set subnet mask to match your network
  WiFi.config(ip, gateway, subnet);

  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());         // Send the IP address of the ESP8266 to the computer
}


// --- Neopixels function ---
void theaterChase(uint32_t color, int wait) {
  for(int a=0; a<10; a++) {  // Repeat 10 times...
    for(int b=0; b<3; b++) { //  'b' counts from 0 to 2...
      strip.clear();         //   Set all pixels in RAM to 0 (off)
      // 'c' counts up from 'b' to end of strip in steps of 3...
      for(int c=b; c<strip.numPixels(); c += 3) {
        strip.setPixelColor(c, color); // Set pixel 'c' to value 'color'
      }
      strip.show(); // Update strip with new contents
      delay(wait);  // Pause for a moment
    }
  }
}

// --- Loop function ---
void loop() {
  
  // Display the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);
  Serial.print("Button state:");
  Serial.println(buttonState);

  //Display the selected sensor
  if (buttonState != 0 && (sensorChoice.indexOf("Temperature") >= 0)) {
    sensorChoice = "Humidite";
  }
  else if (buttonState != 0 && (sensorChoice.indexOf("Humidite") >= 0)) {
    sensorChoice = "Temperature";
  }
  Serial.print("sensorChoice:");
  Serial.println(sensorChoice);

  // Activate LED for sensor selection (temp: blue, humidity: red)
  if (sensorChoice.indexOf("Temperature") >= 0) {
    digitalWrite(D8, HIGH);
    digitalWrite(D7, LOW);
  }
  else if (sensorChoice.indexOf("Humidite") >= 0) {
    digitalWrite(D7, HIGH);
    digitalWrite(D8, LOW);
  }

  // Connect to server
  WiFiClient client;

  Serial.printf("\n[Connecting to server...");
  if (client.connect(server, 80))
  {
    Serial.println("connected]");

    Serial.println("[Sending a request]");
    client.print("Demande de data du client sur: ");
    client.print(sensorChoice);
    client.print("\r\n");
    client.print("\r\n");

    // Read response from server 
    //client.available() client disponnible   
    Serial.println("[Response:]");
    while (client.connected() || client.available())
    {
      if (client.available())
      {
        String line = client.readStringUntil('\n');
        Serial.println(line);
        tempInt = line.toInt();
        if(line.indexOf("Temperature") >= 0) {
          sensorInfo = 0;
        }
        else if (line.indexOf("Humidite") >= 0){
          sensorInfo = 1;
        }
      }
    }
    client.stop();
    Serial.println("\n[Disconnected]");
    
  }
  else
  {
    Serial.println("connection failed!]");
    client.stop();
  }

  // Display colors on Neopixels
  if (sensorInfo == 0) {    //Si mesure de temperature
    Serial.println(tempInt);
    if  (tempInt > 27) {
      theaterChase(strip.Color(255, 0, 0), 50);   //couleur rouge
    }
    else if (tempInt < 24) {
      theaterChase(strip.Color(75, 0, 130), 50);   //couleur violet
    }
    else {
      theaterChase(strip.Color(255, 165, 0), 50);   //couleur orange
    }
  }
  else if (sensorInfo == 1) {   //Si mesure d'humidite
    Serial.println(tempInt);
    if  (tempInt > 88) {
      theaterChase(strip.Color(0, 0, 100), 50);   //couleur bleu fonce
    }
    else if (tempInt < 65) {
      theaterChase(strip.Color(150, 220, 255), 50);   // couleur bleu tres clair
    }
    else {
      theaterChase(strip.Color(0, 128, 0), 50);  //couleur vert
    }
  }
}
