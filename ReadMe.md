# Sensor Wifi connect.

## 2 ESP communiquent en Wifi.
- Plan du projet :
![plant](plan.jpg "plan")
- Schéma du electronique  esp 1
![Serveur](esp_accessPoint.png  "schéma 1")
- Schéma du electronique  esp 2
![client 2](esp_Client.png "schéma 2")
- Le premier ESP nous sert de serveur, il dispose d'un capteur de température et d'humidité :
![Serveur](espA.jpg "Serveur")
- Le second ESP est nous sert comme client, il nous permet de recuperer les informations du serveur et de faire un traitement de cette donnée selon les informations recues il change la couleur des Neopixels.
![Serveur](espC.jpg "Client")
- On utilise 1 bouton pour faire un switch entre la température et l'humidité, ainsi que 2 LED qui vont indiquer si température ou humidité est sélectionné
![Serveur](espC.jpg "Client")
- pour finir on utilise un des lumieres led  (Neopixels) pour traiter les données recues sur le client de cette façon :
![Serveur](espC.jpg "schéma 1")


